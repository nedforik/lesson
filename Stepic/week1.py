"""
# 2.3 prog1
# На вход подаётся строка из 3 целых чисел.
# Найдите сумму чисел.

print(sum(map(int ,input().split())))
"""
'''
# 2.3 prog2
# Считайте 2 строки. Каждая строка содержит по 3 натуральных числа.
# Найдите сумму чисел в каждой строке и выведите на печать одной строкой.
# В качестве разделителя между двумя получившимися числами используйте символ "#"

list_a = map(int ,input().split())
list_b = map(int ,input().split())
sum_a = sum(list_a)
sum_b = sum(list_b)
print("#".join([str(sum_a), str(sum_b)]))
'''
'''
#2.3 prog3
# На вход подаётся 1 строка, слова в которой разделены символом "&".
# Необходимо вывести все слова из строки (без дубликатов) с пробелом в качестве разделителя.

print(' '.join(set(input().split('&'))))
'''
'''
# 2.3 prog4
# На вход подаётся строка, слова в которой разделены между собой пробелами.
# Напечатайте 2, 3 и предпоследнее слова из строки (через пробел)

string = input().split()
print(string[1], string[2], string[-2])
'''
'''
# 2.3 prog5
# На вход подаётся 1 строка, слова в которой разделены пробелами.
# Напечатайте слова из этой строки в обратном порядке, используя в качестве разделителя "-$-"

string = input().split()
for i in range(len(string)):
    if i != 0:
        string[i] = string[i] + '-$-'
print(''.join(string[::-1]))
'''
'''
# 2.3 prog6
# На вход подаётся 1 строка, слова в которой разделены пробелами.
# Напечатайте через пробел число слов в строке и число слов "one" в строке.

List1 = input().split()
print(len(List1) ,List1.count("one"))
'''
'''
# 2.3 prog7
# читайте строку, содержащую произвольное количество целых чисел и найдите их сумму.

L1 = map(int ,input().split())
print(sum(L1))
'''
'''--------------STRING----------------------------'''
'''
#2.4 prog1
# Считайте текст с клавиатуры (из стандартного потока ввода) и выведите его на печать.
print(input())
'''
'''
# 2.4 prog 2
#Считайте 2 строки и выведите их на печать, разделив символом $.
print(input(), input(), sep='$')
'''
'''
# 2.4 prog 3
# Переменная B1 - строка.
# Запишите в переменную B2 строку, состоящую только из символов строки B1 с нечётными индексами
B1 = 'Hello, world'
B2 = B1[1::2]
'''
'''
# 2.4 prog 4
# Переменная L - строка.
# Запишите в переменную C2 предпоследний символ переменной L
C2 = C[-2]
'''
'''
# 2.4 prog 5
# Вывести строку как список
L = input().split("'")
while True:
    if L.count("[") != 0:
        L.remove("[")
    if L.count("]") != 0:
        L.remove("]")
    if L.count(", ") != 0:
        L.remove(", ")
    if L.count("[") == 0 and L.count("]") == 0 and L.count(", ") == 0:
        break
'''
'''
# 2.5 prog 1
# Считайте число (см таблицу) в арабской записи и выведите на печать, соответствующую ему римскую цифру:
# 1 - I
# 5 - V
#  10 - X
# 50 - L
#  100 - C
#  500 - D
# 1000 - M
number = int(input())
if number == 1:
    print("I")
elif number == 5:
    print("V")
elif number == 10:
    print("X")
elif number == 50:
    print("L")
elif number == 100:
    print("C")
elif number == 500:
    print("D")
elif number == 1000:
    print("M")
'''
'''
# 2.5 prog 2
# Считайте строку.
# Если она содержит число 0, выведите на печать фразу "Division by zero!"
# Если она содержит число, отличное от 0, то считайте ещё одну строку, содержащую число, и напечатайте результат деления 2-ого 
# числа на 1-е. Результат деления округлите до 1 знака после запятой (используйте round).
a = int(input())
if a == 0:
    print("Division by zero!")
else:
    b = int(input())
    print(round(b / a, 1))
'''
'''
# 2.5 prog 3
typ = input()
if "int" == typ:
    a = int(input())
    b = int(input())
    if a == False and b == False:
        print("Empty Ints")
    else:
        print(a + b)
elif "str" == typ:
    a = input()
    if a:
        print(a)
    else:
        print("Empty String")
elif "list" == typ:
    L1 = input().split()
    if L1:
        print(L1[-1])
    else:
        print("Empty List")
else:
    print("Unknown type")
'''
'''
# 2.6 prog 1
a1 = int(input())
a = 0
while a <= a1:
    print(a)
    a += 1

# или
[print(i) for i in range(int(input()) + 1)]
'''
'''
# 2.6 prog 2
# Считайте натуральное число n.
# Выведите на печать все квадраты чётных чисел от 0 до n-1 (включительно), каждый с новой строки.
for i in range(0, int(input()), 2):
    print(i ** 2)
[print(i ** 2) for i in range(0, int(input()), 2)]
'''
'''
# 2.6 prog 3
# Считывайте целые числа из потока ввода по одному до тех пор, пока не встретите строку "The End".
# Выведите на печать сумму считанных чисел.
S = 0
while True:
    i = input()
    if i == "The End":
        break
    S = S + int(i)
print(S)
'''
'''
# 2.6 prog 4
# На вход подаётся строка, некоторые слова в которой "испорчены".
# Признак "испорченного" слова - 1я буква в нём заменена на *.
# Выведете на печать только "не испорченные" слова, каждое с новой строки
L = input().split()
for i in L:
    if i[0] == "*":
        continue
    print(i)
'''
'''
# 2.6 prog 5
# Считайте натуральное число n
# Найдите самый маленький натуральный делитель числа n, отличный от 1 (2 <= n <= 30000). 
n = int(input())
i = 2
while True:
    if n % i == 0:
        break
    i += 1
print(i)
'''
'''
# 2.6 prog 6
# Считайте целое число n
# Напечатайте кубы всех натуральных чисел, меньших |n|, каждое с новой строки.
n = abs(int(input()))
i = 1
while i < n:
    print(i ** 3)
    i += 1
'''
'''
# 2.7 prog 1
# Напишите функцию sum2(a,b), которая возвращает сумму 2 чисел.
def sum2(a, b):
    return a + b
'''
'''
# 2.7 prog 2
# Напишите функцию Hello, которая принимает 1 необязательный параметр:
# Если параметр не задан, то функция выводит на печать сообщение "Hello, %UserName%!"
# Если параметр задан, то вместо %UserName% в приветствии выводится переданное значение
def Hello(UserName = "%UserName%"):
    print("Hello,", UserName + "!")
'''
'''
#2.7 prog 3
# Напишите функцию, для нахождения двойного факториала числа dfactorial(n).
def dfactorial(n):
    if n == 0:
        return 1
    if n <= 2:
        return n
    return n * dfactorial(n - 2)
print(dfactorial(int(input())))
'''
'''
# 2.8 prog 1
# Откройте файл, имя которого передаётся на вход, и выведите содержимое на печать.
f = open(input(), "r", encoding="UTF-8")
print(f.read())
f.close()
'''
'''
# 2.8 prog 2
# Откройте файл, имя которого передаётся на вход.
# Файл содержит произвольное количество целых чисел, каждое с новой строки.
# Выведите на печать сумму всех целых чисел, записанных в файле.
f = open(input(), "r", encoding="UTF-8")
print(sum([int(element) for element in f.readlines()]))
f.close()
'''
'''
# 2.8 prog 3
# Откройте файл, имя которого передаётся на вход.
# Выведите на печать предпоследнюю строку.
# Примечание1. Последняя строка может заканчиваться символом переноса строки.
# Примечание2. Если в конце файла 2 символа переноса строки, значит последняя строка будет содержать ТОЛЬКО символ переноса строки.
f = open(input(), "r", encoding="UTF-8")
l = f.readlines()
if l[-1] == "\n\n":
    print(l[-3])
print(l[-2])
'''
'''
# 2.8 prog 4
# Представьте себя сотрудником деканата. Преподаватели предоставляют вам по 2 файла, имена которых находятся в переменных:
#    sheet - экзаменационную ведомость с фамилиями, именами и отчествами (если есть), оценками и статусами
#    mean - среднюю оценку по группе
# Ведомость может выглядеть следующим образом:
# Аттила 2 (экзамен)
# Бонапарт Наполеон (неявка)
# Гассан Абдуррахман ибн Хоттаб 5 (автомат)
# Задойный Алексей Владимирович 5 (экзамен)
# Колонна-Валевский Александр Флориан Жозеф (недопуск)
# Цезарь Гай Юлий 4 (экзамен)
# Последнее значение (в скобках) - статус:
#     экзамен - человек пришёл на экзамен и получил там оценку
#     автомат - человек хорошо учился и получил оценку "автоматом" до экзамена
#     неявка - человек не пришёл на экзамен и у него нет оценки
#     недопуск - человек не сдал зачёт по физкультуре, а потому его не допустили к нашему экзамену
# Средняя оценка вычисляется только для тех, кто сдавал экзамен очно или получил автомат
# Ваша задача считать данные по всем студентам из файла, вычислить средний балл и сравнить его с тем, что указал преподаватель в mean.
# Если средний балл верен - напечатать "OK"
# Если средний балл рассчитан с ошибкой - напечатать "ERROR"
f1 = open(sheet, "r", encoding="UTF-8")
f2 = open(mean, "r", encoding="UTF-8")
l1 = [element.split() for element in f1.readlines()]
rating = 0
quantity_of_rating = 0
for element in l1:
    if element[-1] == "(экзамен)" or element[-1] == "(автомат)":
        rating += int(element[-2])
        quantity_of_rating += 1
mean_of_rating = rating / quantity_of_rating
if mean_of_rating == int(f2.readline()):
    print("OK")
else:
    print("ERROR") 
f1.close(), f2.close()
'''
'''
# 2.8 prog 5
# На вход подаётся полный путь к файлу относительно текущего каталога.
# Проверьте есть ли такой файл (и файл ли это) и если он есть - выведите содержимое. Иначе выведите одну из 2 ошибок.
import os.path
p = input()
if os.path.exists(p) and os.path.isfile(p):
    with open(p, "r", encoding="UTF-8") as f:
        print("CONTENT:")
        print(f.read())
elif os.path.exists(p):
    print("ERROR:", "Это каталог, а не файл", sep="\n")
else:
    print("ERROR:", "Файл не существует", sep="\n")
'''
'''
# 2.8 prog 6
# Считайте строку, подаваемую на вход и запишите её в файл "output.txt".
with open("output.txt", "w", encoding="UTF-8") as f:
    f.writelines(input())
'''
'''
# 3.2 prog1
# Реализуйте функцию f(x):
# Найдите предел функции (ответ округлите до 3 знака после запятой)  при x→+∞
from math import atan
def f(x):
    return 2 * atan(x)
lim = f(1000000)
print(round(lim, 3))
'''
'''
# 3.2 prog 2
from math import exp
def def_e(x):
    return round((exp(x + 0.000001) - exp(x)) / 0.000001 , 3)
'''
'''
# 3.2 prog 3
L =  [1, 1, 2, 3, 5, 8, 13, 21, 34]
def even_indeces(L):
    return L[::2]
M = even_indeces(L)
print()
'''
'''
# 3.2 prog 4
# Напишите функцию even_elements(l), которая возвращает только чётные элементы списка.
def even_elements(L):
    return [element for element in L if (element % 2) == 0]

L = [1, 1, 2, 3, 5, 8, 13, 21, 34]
M = even_elements(L)
print(M)
'''
'''
# 3.2 PROG 5
# Напишите функцию last_to_first(l), которая вернёт список в обратном порядке. 
def last_to_first(L):
    return L[::-1]

L = [1, 1, 2, 3, 5, 8, 13, 21, 34]
M = last_to_first(L)
print(M)
'''
'''
# 3.2 prog 6
# Считайте строку (используйте input()), содержащую 1 натуральное число n
# Найдите сумму всех чисел от 0 до n, которые делятся на 5, но не делятся на 3 
n = int(input())
print(sum([element for element in range(n + 1) if element % 5 == 0 and element % 3 != 0]))
'''
'''
# 3.2 prog 7
# Напишите функцию common(list_a, list_b), возвращающую список общих элементов 2 списков (порядок не важен).
def common(list_a, list_b):
    return list(set(list_a) & set(list_b))
'''
'''
# 3.2 prog 8
# Напишите функцию front_x(words), которая на вход принимает список строк и возвращает отсортированный по правилам:
# 1) слова, начинающиеся с символа "x", идут первыми
# 2) в лексикографическом порядке.
def front_x(words):
    x_first = [element for element in words if len(element) != 0 and element[0] == "x"]
    empty_symbol = [element for element in words if len(element) == 0]
    over_symbol = [element for element in words if len(element) != 0 and element[0] != "x"]
    return sorted(x_first) + empty_symbol + sorted(over_symbol)
words = ['mix', 'extra', '', 'x-files', 'xyz', 'xapple', 'apple']
M = front_x(words)
print(M)
'''
## Какие-то функции
'''
def numerics(n):
    return list(map(int ,list(str(n))[::-1]))


def kaprekar_step(L):
    return abs(int("".join(list(map(str, sorted(L))))) - int("".join(list(map(str, sorted(L, reverse=True))))))


def kaprekar_check(n):
    if set(numerics(n)) == []:
        return


def kaprekar_loop(n):
    if n <= 1000: 
        print("Ошибка! На вход подано число 1000")
        return "ERR1"
    elif len(set(numerics(n))) == 1:
        print("Ошибка! На вход подано число 1111 - все цифры одинаковые")
        return "ERR2"
    elif n == 6174:
        print(n)
        return n
    print(n)
    return kaprekar_loop(kaprekar_step(numerics(n)))

a = set([1, 0])
print(a)
'''