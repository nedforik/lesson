import pandas as pd 
import numpy as np
# Task_1

# Импортируйте модуль pandas как pd
'''
import pandas as pd


'''


# Task_2
# Выведите на печать версию модуля pandas
'''
import pandas as pd


print(pd.__version__)
'''


# Task_4
# Создайте Dataframe из словаря data. В качестве подписей строк используйте список labels.
# Результат сохраните в переменную df. 
'''
data = {'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
        'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
        'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
        'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']}

labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

df = pd.DataFrame(data=data, index=labels)
'''


# Task_Nan
#    Переменная df содержит DataFrame.
#    Переменная col содержит имя колонки
#    Переменная row содержит имя ИЛИ индекс строки
# Выведите на печать содержимое ячейки
# Выведите на печать значение из соответсвующей ячейки.
'''
df = pd.DataFrame(data={'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
        'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
        'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
        'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']}, index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])
col = "animal"
row = "e"
print(df.loc[row, col])
'''


# Task_5
# Переменная df содержит DataFrame.
# Выведите на печать:
# число непустых (не null) значений в колонке 'age'
# 75% квантиль для значений в колонке 'age'
'''
df = pd.DataFrame(data={'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
        'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
        'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
        'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']}, index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])

print(df.describe().loc["count", "age"])
print(df.describe().loc["75%", "age"])
'''


# Task_6.1
# Переменная df содержит DataFrame.
# Выведите на печать 3 первых строки.
'''
df = pd.DataFrame(data={'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
        'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
        'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
        'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']}, index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])

print(df.iloc[:3])
'''


# Task_6.2
# Переменная df содержит DataFrame.
# Выведите на печать строки с индексами 0, 2, 3.
'''
df = pd.DataFrame(data={'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
        'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
        'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
        'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']}, index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])

print(df.iloc[[0, 2, 3]])
'''


# Task_7
# Переменная df содержит DataFrame.
# Выведите на печать только столбцы 'name' и 'age' (гарантируется, что они есть)
'''
df = pd.DataFrame(data={'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
        'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
        'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
        'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']}, index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])

<<<<<<< Updated upstream


# Task_8
# Переменная df содержит DataFrame.
# Выведите на печать только столбцы 'name' и 'age' И строки 0, 2, 3 (гарантируется, что они есть)
'''
df = pd.DataFrame({'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
                'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
                'name': ['Murzik', 'Pushok', 'Kaa', 'Bobik', 'Strelka', 'Vaska', 'Kaa2', 'Murka', 'Graf', 'Muhtar'],
                'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
                'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']},
                index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']) 
print(df.loc[df.index[[0, 2, 3]], ['name', 'age']])
'''


# Task_9
# Переменная df содержит DataFrame.
# Выведите на печать только данные, относящиеся к записям, возраст которых больше, указанного в переменное critical_age.

df = pd.DataFrame({'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
                'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
                'name': ['Murzik', 'Pushok', 'Kaa', 'Bobik', 'Strelka', 'Vaska', 'Kaa2', 'Murka', 'Graf', 'Muhtar'],
                'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
                'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']},
                index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])

crtical_age = 3
print(df[df['age'] > crtical_age])
print()
=======
print(df.loc[df.index, ['name', 'age']])
'''


# Task_8
# Переменная df содержит DataFrame.
# Выведите на печать только столбцы 'name' и 'age' И строки 0, 2, 3 (гарантируется, что они есть)
'''
df = pd.DataFrame({'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
                'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
                'name': ['Murzik', 'Pushok', 'Kaa', 'Bobik', 'Strelka', 'Vaska', 'Kaa2', 'Murka', 'Graf', 'Muhtar'],
                'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
                'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']},
                index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']) 
print(df.loc[df.index[[0, 2, 3]], ['name', 'age']])
'''


# Task_9
# Переменная df содержит DataFrame.
# Выведите на печать только данные, относящиеся к записям, возраст которых больше, указанного в переменное critical_age.
'''
df = pd.DataFrame({'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
                'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
                'name': ['Murzik', 'Pushok', 'Kaa', 'Bobik', 'Strelka', 'Vaska', 'Kaa2', 'Murka', 'Graf', 'Muhtar'],
                'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
                'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']},
                index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])

crtical_age = 3
print(df[df.age > crtical_age])
'''


# Task_10
# Переменная df содержит DataFrame.
# Выведите на печать только данные, в которых не заполнен возраст (в графе 'age' стоит null).
'''
df = pd.DataFrame({'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
                'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
                'name': ['Murzik', 'Pushok', 'Kaa', 'Bobik', 'Strelka', 'Vaska', 'Kaa2', 'Murka', 'Graf', 'Muhtar'],
                'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
                'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']},
                index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'])

print(df[df.age.notna() != True])
'''
>>>>>>> Stashed changes
