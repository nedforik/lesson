# Task_1
#Создайте и сохраните:
# в переменную V1 Numpy вектор из этих чисел в том же порядке
# в переменную V2 Numpy вектор, содержащий только предпоследнее число (это должен быть именно вектор!)
# в переменную V3 Numpy вектор из этих чисел в обратном порядке
# в переменную V4 Numpy вектор из этих чисел, начиная с 0-ого, через 2 (т.е. каждое третье число)
# в переменную V5 Numpy вектор, созданный из генератора Range, содержащий столько элементов, сколько было передано чисел на вход

'''
import numpy as np
in_val = list(map(float, input().split(", ")))
V1 = np.array(in_val)
V2 = np.array(in_val[-2])
V3 = np.array(in_val[::-1])
V4 = np.array(in_val[0::3])
V5 = np.array(range(len(in_val)))
'''

# Task_2
# На вход подаётся 2 списка целых чисел (они представляют из себя вектора равной длины, т.е. с одинаковым количеством элементов).
#
# Используя векторные операции создайте и сохраните:
#    в переменную V1 Numpy вектор с числами из 1 строки
#    в переменную V2 Numpy вектор с числами из 2 строки
#    в переменную V3 Numpy вектор с покоординатными суммами V1 и V2
#    в переменную V4 Numpy вектор с покоординатными произведениями каждого второго числа V1 на каждое второе число V2, развёрнутого в обратном порядке
'''
import numpy as np
invar_1 = list(map(int, input().split(", ")))
invar_2 = list(map(int, input().split(", ")))
V1 = np.array(invar_1)
V2 = np.array(invar_2)
V3 = V1 + V2
V4 = V1[::2] * V2[::-2]
'''


'''
# Task_3
# На вход подаётся 2 набора целых чисел.
# Создайте вектор V такой, что он будет содержать числа из 1 набора, делящиеся нацело на предпоследнее число из 2 набора и разделённые на это число.
# Если таких чисел не найдётся, то вектор V будет пустым (т.е. не будет содержать элементов).

import numpy as np

temp_arr_1 = np.array(list(map(int, input().split(", "))))
temp_arr_2 = np.array(list(map(int, input().split(", "))))

V = temp_arr_1[(temp_arr_1 % temp_arr_2[-2] == 0)] / temp_arr_2[-2]
'''

'''
# Task_4_triangle_square
# Найти площадь треугольника по координатам

import numpy as np

def triangle_square(a1, a2, a3):
    a = np.linalg.norm(a2 - a1)
    b = np.linalg.norm(a2 - a3)
    c = np.linalg.norm(a1 - a3)
    S = (1/4) * ((a + b + c)*(b + c - a)*(a + c - b)*(a + b - c)) ** (1/2)
    return S

Square = triangle_square(A1, A2, A3)
print(Square)
'''

'''
# Task_5
# 1. Замените значения в предпоследней строке на значения по формуле:
# sin(x⋅π/6), где x - старое значение
# 2. Замените значения в предпоследнем столбце на значения по формуле:
# e^x
# Примечание. Для ячейки на пересечении предпоследнего столбца и предпоследней строки значение ячейки будет изменено по формуле: 
# e^sin(x⋅π6)

import numpy as np 

))
M1 = M1.astype(float)
M2 = np.copy(M1)
M2[-2] = np.sin((M1[-2] * np.pi) / 6)
M2[:, -2] = np.exp(M2[:, -2])
'''


#____________Numpy_100__________________________
# Task_2
'''
# Вывести версию библиотеки
import numpy as np
print(np.__version__)
'''

# Task_3.1
'''
# Считайте число n
# Создайте в переменной Z Numpy вектор из нулей длины n.
import numpy as np
Z = np.zeros(int(input()))
'''
# Task_3.2
# Считайте строку, где значения заданы через пробел shape и dtype
# Последнее значение в строке может быть либо числом (тогда это последнее значение кортежа shape, либо строкой dtype)
# shape - атрибут, задающий размеры матрицы нолей (позволяет вернуть вектор-строку, вектор-столбец, матрицу, куб и т.д.)
# dtype - тип данных, использующихся для значений матрицы. По-умолчанию используется numpy.float64.
# Создайте матрицу Z размера shape, со значениями типа dtype (если dtype не указан, используйте numpy.float64)
'''
import numpy as np


def parameters_definition(String):
    shape = list()
    dtype = list()
    while String[0].isdigit():
        shape.append(int(String[0]))
        String.pop(0)
        if len(String) == 0:
            break
    if len(String):
        dtype = String[0]
        String.pop(0)
    else:
        dtype = float
    return shape, dtype


S = input().split()
a, b = parameters_definition(S)
Z = np.zeros(shape=a, dtype=b)
'''


# Task_4
# Посчитайте размер матрицы Z в байтах и выведите его на печать.
'''
import numpy as np


Z = np.zeros((10,10))
Z.nbytes
print(Z.nbytes)
'''


# Task_5
# Вывести на печать встроенную документацию для функции add numpy, а потом объекта numpy.array.
'''
import numpy as np


np.info(np.add)
np.info(np.array)
'''


# Task_6
# Считайте 2 числа:
# n - размер Numpy вектора
# x - координата элемента вектора, который должен быть равен 1. Остальные элементы вектора должны быть равны 0.
# Сохраните вектор в переменную Z.
'''
import numpy as np


n = int(input())
i = int(input())
Z = np.zeros(n)
Z[i] = 1
'''


# Task_7
# Считайте 2 числа n, m.
# Создайте вектор Z состоящий из чисел от n до m с шагом 1.
'''
import numpy as np


n = int(input())
m = int(input())
Z = np.arange(n, m+1)
'''


# Task_8
# Дан вектор Z
# "Разверните" его.
'''
import numpy as np

Z = np.array([1, 2, 3, 4])
Z = Z[::-1]
'''


# Task_9
# Считайте 3 числа:
# n - количество элементов матрицы
# m и l - размеры матрицы (число строк и столбцов соответственно)
# Заполните матрицу Z числами от 0 до n-1 по порядку (сперва строки, потом столбцы).
# Гарантируется, что m*l = n, т.е. все элементы "влезут" в матрицу и не останется пустых мест.
'''
import numpy as np

n = int(input())
m_l = list(map(int, input().split()))
Z = np.arange(n).reshape(m_l)
'''


# Task_10.1
# Дан вектор Z
# Запишите в переменную NonZerros индексы ненулевых элементов.
# Используйте функцию https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.nonzero.html
'''
import numpy as np

Z = np.array([1, 0, 2, 0, 3, 0, 4])
NonZerros = np.nonzero(Z)
'''


# Task_10.2
# Дана матрица чисел Z (Z может быть 1, 2 или даже 3 мерной).
# Выведите на печать список чисел из этой матрицы, которые больше 3.
'''
import numpy as np
Z = np.array([[[1, 2], [4, 5]], [[7, 8], [0, 0]]])
print(list(Z[np.nonzero(Z > 3)]))
'''


# Task_11
# Считайте число n
# Создайте единичную матрицу размера n, сохраните результат в переменную Z.
'''
import numpy as np
n = int(input())
Z = np.eye(n)
'''


# Task_12
# Считайте 3 числа: n, m, l.
# Зафиксируйте значение генератора случайных чисел Numpy с помощью
# numpy.random.seed(42)
# Создайте матрицу n*m*l из случайных чисел (от 0 до 1) и сохраните результат в переменную Z.
'''
import numpy as np
np.random.seed(42)
n_m_l = tuple(map(int, input().split()))
Z = np.zeros(n_m_l[0] * n_m_l[1] * n_m_l[2])
for i in range(len(Z)):
    Z[i] = np.random.random()
Z = np.reshape(Z, n_m_l)
<<<<<<< HEAD
<<<<<<< b1860fd9b7bcdb33f1408a263c6848383627dbcd
<<<<<<< b1860fd9b7bcdb33f1408a263c6848383627dbcd

print()
=======
'''


# Task_13
# Считайте 2 числа: n, m.
# Зафиксируйте значение генератора случайных чисел Numpy с помощью
# numpy.random.seed(42)
# Создайте матрицу n*m из случайных чисел (от 0 до 1).
# Выведите на печать значение минимального и максимального чисел в получившейся матрице (каждое с новой строки).
'''
import numpy as np

n_m = tuple(map(int, input().split()))
np.random.seed(42)
temp_array = np.zeros(n_m[0] * n_m[1])
for i in range(len(temp_array)):
    temp_array[i] = np.random.random()
print(min(temp_array))
print(max(temp_array))
'''


# Task_14
# Считайте 2 числа: n, m.
# Зафиксируйте значение генератора случайных чисел Numpy с помощью
# numpy.random.seed(42)
# Создайте матрицу n*m из случайных чисел (от 0 до 1).
# Выведите на печать значение среднего для всей матрицы.
'''
import numpy as np

n_m = tuple(map(int, input().split()))
np.random.seed(42)
temp_array = np.zeros(n_m[0] * n_m[1])
for i in range(len(temp_array)):
    temp_array[i] = np.random.random()
print(np.average(temp_array))
'''


# Task_14.1
# Считайте 2 числа: n, m.
# Зафиксируйте значение генератора случайных чисел Numpy с помощью
# numpy.random.seed(42)
# Создайте матрицу n*m из случайных чисел (от 0 до 1).
# Найдите среднее значение для каждого из столбцов.
# Выведите на печать значение минимального и максимального среднего по столбцам (каждое с новой строки).
'''
import numpy as np

n_m = tuple(map(int, input().split()))
np.random.seed(42)
temp_array = np.zeros(n_m[0] * n_m[1])
for i in range(len(temp_array)):
    temp_array[i] = np.random.random()
temp_array = np.reshape(temp_array, n_m)
average_array = np.average(temp_array, axis=0)
print(min(average_array))
print(max(average_array))
'''


# Task_15
# Считайте 2 числа: n, m.
# Создайте матрицу размера n*m такую что:
#    На границе матрицы будут стоять 1
#    Внутри матрицы будут стоять 0
# Сохраните матрицу в переменную Z.
'''
import numpy as np

n_m = tuple(map(int, input().split()))
Z = np.zeros(n_m)
Z[0] = Z[n_m[0]-1] = Z[:, 0] =Z[:, -1] = 1.
'''


# Task_16
# Имеется матрица Z
# Добавьте вокруг имеющихся значений матрицы "забор" из 0
'''
import numpy as np
# test matrix
Z = np.array(
[[1, 2, 3],
[4, 5, 6],
[7, 8, 9]])
Z = np.pad(Z, (1), mode='constant')
'''
'''
import numpy
a = numpy.nan - numpy.nan
b = numpy.nan in set([numpy.nan])
c = 0.3 == 3 * 0.1
d = numpy.nan == numpy.nan
e = 0 * numpy.nan
f = numpy.isnan(numpy.nan)
g = numpy.inf > numpy.nan
'''


# Task_18.1
# Считайте число n.
# Создайте диагональную матрицу размера n*n. На главной диагонали должны быть числа от 1 до n.
# Сохраните матрицу в переменную Z.
'''
import numpy as np

n = int(input())
Z = np.zeros((n, n), dtype=int)
for i in range(1,n+1):
    Z[i-1, i-1] = i
'''


# Task_18.2
# Считайте 2 числа:
#    x - сдвиг для единственной ненулевой диагонали в матрице.
#    k - верхняя граница для интервала чисел на диагонали (т.е. если k = 5, то на диагонали будут стоять числа 1, 2, 3, 4 и 5)
# Договоримся о значениях x:
#    x = 0 - это главная диагональ матрицы
#    x<0 - диагональ сдвигается вниз на |x| ячеек (если x = -1, то первая не нулевая ячейка будет с координатами (1,0))
#    x>0 - диагональ сдвигается вправо на |x| ячеек (если x = 1, то первая ненулевая ячейка будет с координатами (0,1))  
'''
import numpy as np

input_array = list(map(int, input().split()))
k = input_array[0] 
x = input_array[1]
Z = np.diag(np.arange(1, x+1), k=k)
'''


# Task_19
# Считайте 2 числа: n, m.
# Создайте матрицу размера n*m и "раскрасьте" её в шахматную раскраску.
#    0 - "чёрное"
#    1 - "белое"
# Ячейка с координатами (0, 0) всегда "чёрная" (т.е. элемент (0, 0) равен 0).
# Матрицу сохраните в переменную Z.

import numpy as np

 